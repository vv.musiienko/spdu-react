import React, {ReactNode} from 'react';

type Props = {
    children: ReactNode
}

export const Item = (props: Props) => {
    return <li className="list__item">{props.children}</li>
};
