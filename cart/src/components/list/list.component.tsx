import React, {ReactNode} from 'react';
import './list.scss';

type Props = {
    children: ReactNode
};

export const List = (props: Props) => {
    return (
        <ul className="list">
            {props.children}
        </ul>
    )
};
