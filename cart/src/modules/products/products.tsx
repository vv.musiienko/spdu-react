import React from 'react';
import {List, Item} from '../../components/list';
import {Product} from "../typedef";
import './product.scss';

type Props = {
    items: Product[],
    onAdd: (id: string) => void
};

export const Products = (props: Props) => {
    return (
        <List>
            Products
            {props.items.map(product => {
                return (
                    <Item key={product.id}>
                        <div className="product">
                            <img className="product__image" src={product.imgUrl} alt={product.name}/>
                            <h3>{product.name}</h3>
                            <div>{product.description}</div>
                            <strong>${product.price}</strong>
                            <button onClick={() => props.onAdd(product.id)}>Add to cart</button>
                        </div>
                    </Item>
                );
            })}
        </List>
    );
};
