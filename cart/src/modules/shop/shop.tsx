import React, {useState} from 'react';
import {Products} from "../products/products";
import {data} from '../data';
import {Cart} from "../cart/cart";
import './shop.scss';

type CartItems = {
    [id: string]: number
};

export const Shop = () => {
    const [cartItems, setCartItems] = useState<CartItems>({});

    const onItemAdd = (id: string) => {
        const cart = {
            ...cartItems,
            [id]: cartItems[id] ? cartItems[id] + 1 : 1
        };
        setCartItems(cart);
    };

    const onRemoveItem = (id: string) => {
        const newCart = {...cartItems};

        delete newCart[id];
        setCartItems(newCart);
    };

    const cartData = data.filter(item => Object.keys(cartItems).includes(item.id));

    return (
        <div className="shop">
            <Products items={data} onAdd={onItemAdd}/>
            <Cart items={cartData} onRemove={onRemoveItem}/>
        </div>
    )
};
