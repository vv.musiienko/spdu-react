import React from 'react';
import {Item, List} from "../../components/list";
import {Product} from "../typedef";

type Props = {
    items: Product[],
    onRemove: (id: string) => void
}

export const Cart = (props: Props) => {
    const total = props.items.reduce((sum, item) => sum + item.price, 0);

    return (
        <div>
            Cart
            {props.items.length === 0
                ? <div>No data to display</div>
                : (
                    <List>
                        {props.items.map(product => (
                            <Item key={product.id}>
                                <div className="product">
                                    <img className="product__image" src={product.imgUrl} alt={product.name}/>
                                    <h3>{product.name}</h3>
                                    <strong>${product.price}</strong>
                                    <button onClick={() => props.onRemove(product.id)}>Remove</button>
                                </div>
                            </Item>
                        ))}
                    </List>
                )
            }

            {total ? <span>Sum: $ {total}</span> : null}
        </div>
    );
};



