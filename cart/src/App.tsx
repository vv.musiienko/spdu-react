import React from 'react';
import './App.css';
import {Shop} from "./modules/shop/shop";

const App: React.FC = () => {
    return (
        <div className="App">
            <Shop/>
        </div>
    );
};

export default App;
